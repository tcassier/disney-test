import { combineReducers } from 'redux';

import ItemsReducer from "./items_reducer";

export default combineReducers({
  items: ItemsReducer
});