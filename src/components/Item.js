import React from "react";

import "./item.css";

const Item = ({ item }) => {
  return (
    <div className="item">
      <img className="item-img" src={item.img} alt="img" />
      <div className="item-text-container">
        <div className="item-header-container">
          <div className="item-title">{item.title}</div>
          <div className="item-price">{item.price} €<div className="item-price-subtitle">prix/nuit</div></div>
        </div>
        <div className="item-description">{item.description}</div>
      </div>
    </div>
  );
};

export default Item;