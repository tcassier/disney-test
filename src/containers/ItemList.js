import React, { Component } from "react";

import { connect } from "react-redux";
import { getItems } from "../actions";

import Item from "../components/Item";

class ItemList extends Component {
  componentDidMount() {
    this.props.getItems();
  }

  render() {
    const { items } = this.props;

    return (
      <div>
        {
          items.map(item => {
            return (
              <Item key={item.id} item={item} />
            );
          })
        }
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    items: state.items
  }
}

export default connect(mapStateToProps, { getItems })(ItemList);