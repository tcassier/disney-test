import img1 from "../imgs/1.jpg";
import img2 from "../imgs/2.jpg";
import img3 from "../imgs/3.jpg";
import img4 from "../imgs/4.jpg";
import img5 from "../imgs/5.jpg";

export const GET_ITEMS = "GET_items";

export function getItems() {
  return {
    type: GET_ITEMS,
    payload: [
      {
        id: "1",
        img: img1,
        title: "Hotel Cabana",
        price: "400",
        description: "Tum est Cato locutus, quo erat nemo fere senior temporibus illis, nemo prudentior; nunc Laelius et sapiens (sic enim est habitus) et amicitiae gloria excellens de amicitia loquetur."
      },
      {
        id: "2",
        img: img2,
        title: "Hotel Ibis",
        price: "50",
        description: "Mox dicta finierat, multitudo omnis ad, quae imperator voluit, promptior laudato consilio consensit in pacem ea ratione maxime percita, quod norat expeditionibus crebris fortunam eius in malis tantum civilibus vigilasse, cum autem bella moverentur externa."
      },
      {
        id: "3",
        img: img3,
        title: "France Hotel",
        price: "750",
        description: "Qui eum adulabili sermone seriis admixto solus omnium proficisci pellexit vultu adsimulato saepius replicando quod flagrantibus votis eum videre frater cuperet patruelis."
      },
      {
        id: "4",
        img: img4,
        title: "Angelica Hotel",
        price: "320",
        description: "Principium autem unde latius se funditabat, emersit ex negotio tali. Chilo ex vicario et coniux eius Maxima nomine, questi apud Olybrium ea tempestate urbi praefectum, vitamque suam venenis petitam adseverantes inpetrarunt ut hi, quos suspectati sunt."
      },
      {
        id: "5",
        img: img5,
        title: "Hotel du Sauvage",
        price: "45",
        description: "Quare hoc quidem praeceptum, cuiuscumque est, ad tollendam amicitiam valet; illud potius praecipiendum fuit, ut eam diligentiam adhiberemus in amicitiis comparandis, ut ne quando amare inciperemus eum, quem aliquando odisse possemus."
      }
    ]
  }
}