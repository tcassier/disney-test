import React from "react";
import "./App.css";

import Header from "./components/Header";
import ItemList from "./containers/ItemList";
import Cart from "./containers/Cart";

function App() {
  return (
    <div className="App">
      <Header />
      <div className="main-container">
        <div className="item-list-container">
          <ItemList />
        </div>
        <div className="cart-container">
          <Cart />
        </div>
      </div>
    </div>
  );
}

export default App;
